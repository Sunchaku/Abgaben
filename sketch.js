'use strict';
/**
 * Variablen
 */
var imgBackground;
var imagesPlayer = [];
var players = [];
var items = [];
var NUMBER_ITEMS = 5;
var imgItem;
var movingStrategies = [mouseMover, randomMover, horizontalMover];
var distCollission = 50;
/**
 * Lädt die benötigten Bilder
 */
function preload() {
    imgBackground = loadImage("assets/background.jpg");
    imagesPlayer.push(loadImage("assets/player1.jpg"));
    imagesPlayer.push(loadImage("assets/player2.jpg"));
    imagesPlayer.push(loadImage("assets/player3.jpg"));
    imgItem = loadImage("assets/item.jpg");
    console.log("end of preload");
}
/**
 * Legt die Fenstergröße fest, lädt das Hintergrundbild und "kreiert" 3 Player und 5 Items
 */
function setup() {
    createCanvas(800, 400);
    background(imgBackground);
    for (var i = 0; i < Math.min(imagesPlayer.length, movingStrategies.length); i++)
        players.push(new Player(i, imagesPlayer[i], movingStrategies[i]));
    for (var i = 0; i < NUMBER_ITEMS; i++)
        items.push(new Item());
    console.log("end of setup");
}
/**
 * Lädt (ebenfalls) das Hintergrundbild, zeichnet die Player, legt deren Bewegung fest (Aufruf Funktion update()),
 * und macht eine Collision-Abfrage
 * (Wenn ein Player und ein Item kollidieren, wird das Item "gegessen" und der Score des Players wird erhöht,
 * ebenfalls spawned ein neues Item)
 */
function draw() {
    background(imgBackground);
    for (var i = 0; i < players.length; i++) {
        for (var k = 0; k < items.length; k++) {
            var d = distFixed(items[k].position, players[i].position);
            if (d < distCollission) {
                players[i].scored();
                items[k].eaten();
            }
            items[k].draw();
        }
        players[i].update();
        players[i].draw();
    }
}
/**
 *
 * Funktion zur Berechnung der Distanz zwischen Item und Player
 *
 * @param v1 - Position Item
 * @param v2 - Position Player
 * @returns {number} - Distanz
 */
function distFixed(v1, v2) {
    return Math.sqrt((v2.x - v1.x) * (v2.x - v1.x) + (v2.y - v1.y) * (v2.y - v1.y));
}
/**
 *
 * Funktion für den Player, welcher sich immer dort befindet, wo der Mauszeiger ist (Bewegungsstrategie 1)
 *
 * @param x - Position des Mauszeigers auf der x-Achse
 * @param y - Position des Mauszeigers auf der y-Achse
 * @returns {{x: number, y: number}} - Position Mauszeiger x/y
 */
function mouseMover(x, y) {
    return {x: mouseX, y: mouseY};
}
/**
 *
 * Funktion zur willkürlichen Bewegung eines Players (Bewegungsstrategie 2)
 *
 * @param xOld - alte x-Position
 * @param yOld - alte y-Position
 * @returns {{x: Number, y: Number}} - neue Positionen auf beiden Achsen (alte Positionen + willkürliche Geschwindigkeiten)
 */
function randomMover(xOld, yOld) {
    var maxSpeed = 10;
    return {x: constrain(xOld + random(-maxSpeed, maxSpeed), 0, width), y: constrain(yOld + random(-maxSpeed, maxSpeed), 0, height)};
}
/**
 *
 * Funktion für einen sich permanent auf der x-Achse (horizontal) bewegenden Player (Bewegungsstrategie 3)
 *
 * @param xOld - alte x-Position
 * @param yOld - alte y-Position
 * @returns {{x: number, y: *}} - neue x-Position (alte Posotion + Geschwindigkeit 1), y bleibt gleich
 */
function horizontalMover(xOld, yOld) {
    return {x: xOld > width ? 0 : xOld + 1, y: yOld};
}